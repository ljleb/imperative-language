#ifndef AST_HPP
#define AST_HPP

#include <visitor.hpp>
#include <any>

namespace imp
{
    struct Ast
    {
        virtual ~Ast() = default;

        template <typename _Visitor, typename... _Args>
        typename _Visitor::PartialOutput accept(_Args&&... args) const&
        {
            _Visitor const visitor { std::forward<_Args>(args)... };
            std::any const&& any_output { accept_any(visitor) };
            if (!any_output.has_value())
            {
                std::string const&& message { "visitor to implement `accept(" + std::string { typeid(this).name() } + ")`" };
                return _Visitor::PartialOutput::leftOf(message);
            }
            return std::any_cast<typename _Visitor::PartialOutput>(any_output);
        }

    protected:
        virtual std::any accept_any(AnyVisitor const& visitor) const& = 0;
    };

    template <typename _This, typename _Base = Ast>
    struct AstImpl : public _Base
    {
    private:
        virtual std::any accept_any(AnyVisitor const& any_visitor) const& final override
        {
            VisitorOf<_This> const* const&& visitor { dynamic_cast<VisitorOf<_This> const*>(&any_visitor) };
            if (visitor == nullptr) return {};
            return visitor->visit_any(static_cast<_This const&&>(*this));
        }
    };
}

#endif
