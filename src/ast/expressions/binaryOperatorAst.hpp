#ifndef BINARY_OPERATOR_AST_HPP
#define BINARY_OPERATOR_AST_HPP

#include <ast/expressionAst.hpp>
#include <ast/operatorAst.hpp>

#include <memory>

namespace imp
{
    struct BinaryOperatorAst final : public AstImpl<BinaryOperatorAst, ExpressionAst>
    {
        BinaryOperatorAst(
            std::unique_ptr<OperatorAst const>&& operator_,
            std::unique_ptr<ExpressionAst const>&& left,
            std::unique_ptr<ExpressionAst const>&& right
        ):
            operator_ { std::move(operator_) },
            left { std::move(left) },
            right { std::move(right) }
        {}

        std::unique_ptr<OperatorAst const> operator_;
        std::unique_ptr<ExpressionAst const> left;
        std::unique_ptr<ExpressionAst const> right;
    };
}

#endif
