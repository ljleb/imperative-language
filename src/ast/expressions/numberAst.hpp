#ifndef NUMBER_AST_HPP
#define NUMBER_AST_HPP

#include <ast/expressionAst.hpp>

#include <cstdint>

namespace imp
{
    struct NumberAst final : public AstImpl<NumberAst, ExpressionAst>
    {
        NumberAst(uint64_t&& number):
            number { std::move(number) }
        {}

        uint64_t number;
    };
}

#endif
