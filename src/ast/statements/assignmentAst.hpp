#ifndef ASSIGNMENT_AST_HPP
#define ASSIGNMENT_AST_HPP

#include <ast/statementAst.hpp>
#include <ast/expressionAst.hpp>

#include <string_view>
#include <memory>

namespace imp
{
    struct AssignmentAst final : public AstImpl<AssignmentAst, StatementAst>
    {
        AssignmentAst(
            std::string_view&& name,
            std::unique_ptr<ExpressionAst const>&& expression
        ):
            name { std::move(name) },
            expression { std::move(expression) }
        {}

        std::string_view name;
        std::unique_ptr<ExpressionAst const> expression;
    };
}

#endif
