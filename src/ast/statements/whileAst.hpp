#ifndef WHILE_AST_HPP
#define WHILE_AST_HPP

#include <ast/statementAst.hpp>
#include <ast/expressionAst.hpp>

#include <memory>

namespace imp
{
    struct WhileAst final : public AstImpl<WhileAst, StatementAst>
    {
        WhileAst(
            std::unique_ptr<ExpressionAst const>&& condition,
            std::unique_ptr<StatementAst const>&& statement
        ):
            condition { std::move(condition) },
            statement { std::move(statement) }
        {}

        std::unique_ptr<ExpressionAst const> condition;
        std::unique_ptr<StatementAst const> statement;
    };
}

#endif
