#ifndef CONDITION_AST_HPP
#define CONDITION_AST_HPP

#include <ast/statementAst.hpp>
#include <ast/expressionAst.hpp>

#include <neither/maybe.hpp>
#include <memory>

namespace imp
{
    struct ConditionAst final : public AstImpl<ConditionAst, StatementAst>
    {
        ConditionAst(
            std::unique_ptr<ExpressionAst const>&& condition,
            std::unique_ptr<StatementAst const>&& true_statement,
            std::unique_ptr<StatementAst const>&& false_statement
        ):
            condition { std::move(condition) },
            true_statement { std::move(true_statement) },
            false_statement { std::move(false_statement) }
        {}

        ConditionAst(
            std::unique_ptr<ExpressionAst const>&& condition,
            std::unique_ptr<StatementAst const>&& true_statement
        ):
            condition { std::move(condition) },
            true_statement { std::move(true_statement) },
            false_statement { neither::maybe() }
        {}

        std::unique_ptr<ExpressionAst const> condition;
        std::unique_ptr<StatementAst const> true_statement;
        neither::Maybe<std::unique_ptr<StatementAst const>> false_statement;
    };
}

#endif
