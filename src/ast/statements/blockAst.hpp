#ifndef BLOCK_AST_HPP
#define BLOCK_AST_HPP

#include <ast/statementAst.hpp>

#include <memory>
#include <vector>

namespace imp
{
    struct BlockAst final : public AstImpl<BlockAst, StatementAst>
    {
        BlockAst(std::vector<std::unique_ptr<StatementAst const>>&& statements):
            statements { std::move(statements) }
        {}

        std::vector<std::unique_ptr<StatementAst const>> statements;
    };
}

#endif
