#ifndef FUNCTION_AST_HPP
#define FUNCTION_AST_HPP

#include <ast/statementAst.hpp>

#include <ast.hpp>

#include <string_view>
#include <memory>

namespace imp
{
    struct FunctionAst final : public AstImpl<FunctionAst>
    {
        FunctionAst(
            std::string_view&& name,
            std::string_view&& parameter_name,
            std::unique_ptr<StatementAst const>&& body
        ):
            name { std::move(name) },
            parameter_name { std::move(parameter_name) },
            body { std::move(body) }
        {}

        std::string_view name;
        std::string_view parameter_name;
        std::unique_ptr<StatementAst const> body;
    };
}

#endif
