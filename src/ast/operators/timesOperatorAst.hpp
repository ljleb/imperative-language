#ifndef TIMES_OPERATOR_AST_HPP
#define TIMES_OPERATOR_AST_HPP

#include <ast/operatorAst.hpp>

namespace imp
{
    struct TimesOperatorAst final : public AstImpl<TimesOperatorAst, OperatorAst>
    {};
}

#endif
