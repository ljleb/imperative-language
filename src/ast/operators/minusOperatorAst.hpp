#ifndef MINUS_OPERATOR_AST_HPP
#define MINUS_OPERATOR_AST_HPP

#include <ast/operatorAst.hpp>

namespace imp
{
    struct MinusOperatorAst final : public AstImpl<MinusOperatorAst, OperatorAst>
    {};
}

#endif
