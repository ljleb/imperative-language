#ifndef DIVIDE_OPERATOR_AST_HPP
#define DIVIDE_OPERATOR_AST_HPP

#include <ast/operatorAst.hpp>

namespace imp
{
    struct DivideOperatorAst final : public AstImpl<DivideOperatorAst, OperatorAst>
    {};
}

#endif
