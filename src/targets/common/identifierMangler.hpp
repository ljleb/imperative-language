#ifndef TARGET_IDENTIFIER_MANGLER_HPP
#define TARGET_IDENTIFIER_MANGLER_HPP

#include <string_view>
#include <string>

namespace imp
{
    struct IdentifierMangler
    {
        virtual std::string mangle_variable(std::string_view const& name) const& = 0;
        virtual std::string mangle_function(std::string_view const& name) const& = 0;
    };
}

#endif
