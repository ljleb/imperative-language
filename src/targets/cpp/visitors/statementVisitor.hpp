#ifndef TARGET_CPP_STATEMENT_VISITOR_HPP
#define TARGET_CPP_STATEMENT_VISITOR_HPP

#include "expressionVisitor.hpp"
#include <targets/cpp/controlflowStrategy.hpp>

#include <ast/statements/returnAst.hpp>
#include <ast/statements/assignmentAst.hpp>
#include <ast/statements/blockAst.hpp>
#include <ast/statements/whileAst.hpp>
#include <ast/statements/conditionAst.hpp>
#include <visitor.hpp>

#include <string>

namespace imp
{
    struct StatementVisitor final : public Visitor<std::string, ReturnAst, AssignmentAst, BlockAst, WhileAst, ConditionAst>
    {
        StatementVisitor(
            IdentifierMangler const& identifier_mangler,
            ControlflowStrategy const& controlflow_strategy
        ):
            _identifier_mangler { identifier_mangler },
            _controlflow_strategy { controlflow_strategy }
        {}

        virtual PartialOutput visit(ReturnAst const& ast) const& final override
        {
            auto const&& expression_output { ast.expression->accept<ExpressionVisitor>(_identifier_mangler) };
            if (expression_output.isLeft) return PartialOutput::leftOf(expression_output.leftValue);

            return PartialOutput::rightOf(_controlflow_strategy.get_return_statement(expression_output.rightValue));
        }

        virtual PartialOutput visit(AssignmentAst const& ast) const& final override
        {
            auto const&& expression_output { ast.expression->accept<ExpressionVisitor>(_identifier_mangler) };
            if (expression_output.isLeft) return PartialOutput::leftOf(expression_output.leftValue);

            return PartialOutput::rightOf(_identifier_mangler.mangle_variable(ast.name) + "=" + expression_output.rightValue + ";");
        }

        virtual PartialOutput visit(BlockAst const& ast) const& final override
        {
            std::string target { "{" };

            for (std::unique_ptr<StatementAst const> const& statement: ast.statements)
            {
                auto const&& statement_output { statement->accept<StatementVisitor>(_identifier_mangler, _controlflow_strategy) };
                if (statement_output.isLeft) return PartialOutput::leftOf(statement_output.leftValue);

                target += statement_output.rightValue;
            }

            target += "}";

            return PartialOutput::rightOf(std::move(target));
        }

        virtual PartialOutput visit(WhileAst const& ast) const& final override
        {
            auto const&& condition_output { ast.condition->accept<ExpressionVisitor>(_identifier_mangler) };
            if (condition_output.isLeft) return PartialOutput::leftOf(condition_output.leftValue);

            auto const&& statement_output { ast.statement->accept<StatementVisitor>(_identifier_mangler, _controlflow_strategy) };
            if (statement_output.isLeft) return PartialOutput::leftOf(statement_output.leftValue);

            return PartialOutput::rightOf(
                "while(" + condition_output.rightValue + "){" + statement_output.rightValue + "}");
        }

        virtual PartialOutput visit(ConditionAst const& ast) const& final override
        {
            std::string target { "if(" };

            auto const&& condition_output { ast.condition->accept<ExpressionVisitor>(_identifier_mangler) };
            if (condition_output.isLeft) return PartialOutput::leftOf(condition_output.leftValue);
            target += condition_output.rightValue;

            target += "){";

            auto const&& true_statement_output { ast.true_statement->accept<StatementVisitor>(_identifier_mangler, _controlflow_strategy) };
            if (true_statement_output.isLeft) return PartialOutput::leftOf(true_statement_output.leftValue);
            target += true_statement_output.rightValue;

            target += "}";

            if (ast.false_statement.hasValue)
            {
                target += "else{";

                auto const&& false_statement_output { ast.false_statement.value->accept<StatementVisitor>(_identifier_mangler, _controlflow_strategy) };
                if (false_statement_output.isLeft) return PartialOutput::leftOf(false_statement_output.leftValue);
                target += false_statement_output.rightValue;

                target += "}";
            }

            return PartialOutput::rightOf(target);
        }

    private:
        IdentifierMangler const& _identifier_mangler;
        ControlflowStrategy const& _controlflow_strategy;
    };
}

#endif
