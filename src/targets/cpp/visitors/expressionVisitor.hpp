#ifndef TARGET_CPP_EXPRESSION_VISITOR_HPP
#define TARGET_CPP_EXPRESSION_VISITOR_HPP

#include <ast/expressions/numberAst.hpp>
#include <ast/expressions/binaryOperatorAst.hpp>
#include <ast/expressions/callAst.hpp>
#include <ast/expressions/referenceAst.hpp>
#include <targets/cpp/visitors/operatorVisitor.hpp>

#include <targets/common/identifierMangler.hpp>

#include <visitor.hpp>

#include <string>

namespace imp
{
    struct ExpressionVisitor final : public Visitor<std::string, NumberAst, CallAst, ReferenceAst, BinaryOperatorAst>
    {
        ExpressionVisitor(IdentifierMangler const& identifier_mangler):
            _identifier_mangler { identifier_mangler }
        {}

        virtual PartialOutput visit(NumberAst const& ast) const& final override
        {
            return PartialOutput::rightOf(std::to_string(ast.number));
        }

        virtual PartialOutput visit(CallAst const& ast) const& final override
        {
            auto const&& partial_argument { ast.argument->accept<ExpressionVisitor>(_identifier_mangler) };
            if (partial_argument.isLeft) return PartialOutput::leftOf(partial_argument.leftValue);

            return PartialOutput::rightOf(
                _identifier_mangler.mangle_function(ast.name) +
                "(" +
                partial_argument.rightValue +
                ")");
        }

        virtual PartialOutput visit(ReferenceAst const& ast) const& final override
        {
            return PartialOutput::rightOf(_identifier_mangler.mangle_variable(ast.name));
        }

        virtual PartialOutput visit(BinaryOperatorAst const& ast) const& final override
        {
            auto const& left_target { ast.left->accept<ExpressionVisitor>(_identifier_mangler) };
            if (left_target.isLeft) return PartialOutput::leftOf(left_target.leftValue);

            auto const& operator_target { ast.operator_->accept<OperatorVisitor>() };
            if (operator_target.isLeft) return PartialOutput::leftOf(operator_target.leftValue);

            auto const& right_target { ast.right->accept<ExpressionVisitor>(_identifier_mangler) };
            if (right_target.isLeft) return PartialOutput::leftOf(right_target.leftValue);

            return PartialOutput::rightOf("(" + left_target.rightValue + operator_target.rightValue + right_target.rightValue + ")");
        }

    private:
        IdentifierMangler const& _identifier_mangler;
    };
}

#endif
