#ifndef TARGET_CPP_ROOT_VISITOR_HPP
#define TARGET_CPP_ROOT_VISITOR_HPP

#include "functionVisitor.hpp"

#include <ast/rootAst.hpp>
#include <ast/functionAst.hpp>

#include <visitor.hpp>

#include <vector>
#include <string>

namespace imp
{
    struct RootVisitor final : public Visitor<std::string, RootAst>
    {
        RootVisitor(
            IdentifierMangler const& identifier_mangler,
            std::vector<std::reference_wrapper<ControlflowStrategy const>> const& controlflow_strategies,
            std::vector<std::reference_wrapper<Detion const>> const& detions
        ):
            _identifier_mangler { identifier_mangler },
            _controlflow_strategies { controlflow_strategies },
            _detions { detions }
        {}

        virtual PartialOutput visit(RootAst const& ast) const& final override
        {
            Output output { "#include<iostream>\n#include<string>\n#include<cstdint>\n" };

            for (Detion const& detion: _detions)
            {
                auto const&& forms_target { get_functions_target(ast, detion) };
                if (forms_target.isLeft) return PartialOutput::leftOf(forms_target.leftValue);
                output += forms_target.rightValue;
            }

            return PartialOutput::rightOf(std::move(output));
        }

    private:
        IdentifierMangler const& _identifier_mangler;
        std::vector<std::reference_wrapper<ControlflowStrategy const>> const& _controlflow_strategies;
        std::vector<std::reference_wrapper<Detion const>> const& _detions;

        PartialOutput get_functions_target(RootAst const& ast, Detion const& detion) const&
        {
            Output output;

            for (std::unique_ptr<FunctionAst const> const& function: ast.functions)
            {
                auto const&& function_output
                {
                    function->accept<FunctionVisitor>(detion, _controlflow_strategies)
                };

                if(function_output.isLeft)
                {
                    return PartialOutput::leftOf(function_output.leftValue);
                }

                output += function_output.rightValue;
            }

            return PartialOutput::rightOf(std::move(output));
        }
    };
}

#endif
