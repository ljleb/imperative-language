#ifndef TARGET_CPP_OPERATOR_VISITOR_HPP
#define TARGET_CPP_OPERATOR_VISITOR_HPP

#include <ast/operators/plusOperatorAst.hpp>
#include <ast/operators/minusOperatorAst.hpp>
#include <ast/operators/timesOperatorAst.hpp>
#include <ast/operators/divideOperatorAst.hpp>

#include <targets/common/identifierMangler.hpp>

#include <visitor.hpp>

#include <string>

namespace imp
{
    struct OperatorVisitor final : public Visitor<std::string, PlusOperatorAst, MinusOperatorAst, TimesOperatorAst, DivideOperatorAst>
    {
        virtual PartialOutput visit(PlusOperatorAst const& ast) const& final override
        {
            return PartialOutput::rightOf("+");
        }

        virtual PartialOutput visit(MinusOperatorAst const& ast) const& final override
        {
            return PartialOutput::rightOf("-");
        }

        virtual PartialOutput visit(TimesOperatorAst const& ast) const& final override
        {
            return PartialOutput::rightOf("*");
        }

        virtual PartialOutput visit(DivideOperatorAst const& ast) const& final override
        {
            return PartialOutput::rightOf("/");
        }
    };
}

#endif
