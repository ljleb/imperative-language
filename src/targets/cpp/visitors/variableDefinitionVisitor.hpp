#ifndef TARGET_CPP_VARIABLE_DECLARATION_VISITOR_HPP
#define TARGET_CPP_VARIABLE_DECLARATION_VISITOR_HPP

#include <targets/cpp/visitors/expressionVisitor.hpp>
#include <targets/cpp/controlflowStrategy.hpp>

#include <ast/statements/returnAst.hpp>
#include <ast/statements/assignmentAst.hpp>
#include <ast/statements/blockAst.hpp>
#include <visitor.hpp>

#include <string>

namespace imp
{
    struct VariableDefinitionVisitor final : public Visitor<std::string, BlockAst, AssignmentAst>
    {
        VariableDefinitionVisitor(IdentifierMangler const& identifier_mangler):
            _identifier_mangler { identifier_mangler }
        {}

        virtual PartialOutput visit(AssignmentAst const& ast) const& final override
        {
            return PartialOutput::rightOf("uint64_t " + _identifier_mangler.mangle_variable(ast.name) + "{0};");
        }

        virtual PartialOutput visit(BlockAst const& ast) const& final override
        {
            std::string target;

            for (std::unique_ptr<StatementAst const> const& statement: ast.statements)
            {
                auto const&& variable_declaration_output { statement->accept<VariableDefinitionVisitor>(_identifier_mangler) };
                if (variable_declaration_output.isLeft) continue;

                target += variable_declaration_output.rightValue;
            }

            return PartialOutput::rightOf(std::move(target));
        }

    private:
        IdentifierMangler const& _identifier_mangler;
    };
}

#endif
