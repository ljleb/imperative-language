#ifndef TARGET_CPP_FUNCTION_VISITOR_HPP
#define TARGET_CPP_FUNCTION_VISITOR_HPP

#include <targets/cpp/controlflowStrategy.hpp>
#include <targets/cpp/detion.hpp>

#include <targets/common/identifierMangler.hpp>
#include <ast/functionAst.hpp>
#include <visitor.hpp>

#include <vector>
#include <string>

namespace imp
{
    struct FunctionVisitor : public Visitor<std::string, FunctionAst>
    {
        FunctionVisitor(
            Detion const& detion,
            std::vector<std::reference_wrapper<ControlflowStrategy const>> const& controlflow_strategies
        ):
            _detion { detion },
            _controlflow_strategies { controlflow_strategies }
        {}

        virtual PartialOutput visit(FunctionAst const& ast) const& final override
        {
            for (ControlflowStrategy const& controlflow_strategy: _controlflow_strategies)
            {
                if (controlflow_strategy.accepts(ast))
                {
                    return _detion.get_function_target(ast, controlflow_strategy);
                }
            }
            return PartialOutput::leftOf("at least one controlflow strategy to accept the function");
        }

    private:
        Detion const& _detion;
        std::vector<std::reference_wrapper<ControlflowStrategy const>> const& _controlflow_strategies;
    };
}

#endif
