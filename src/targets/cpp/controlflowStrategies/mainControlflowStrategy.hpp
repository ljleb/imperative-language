#ifndef TARGET_CPP_MAIN_CONTROLFLOW_STRATEGY_HPP
#define TARGET_CPP_MAIN_CONTROLFLOW_STRATEGY_HPP

#include <targets/common/identifierMangler.hpp>

#include <targets/cpp/controlflowStrategy.hpp>

namespace imp
{
    struct MainControlflowStrategy final : public ControlflowStrategy
    {
        MainControlflowStrategy(IdentifierMangler const& identifier_mangler):
            _identifier_mangler { identifier_mangler }
        {}

        virtual bool accepts(FunctionAst const& ast) const& final override
        {
            return ast.name == "main";
        }

        virtual std::string get_signature(FunctionAst const& ast) const& final override
        {
            return "int main(int,char*argv[])";
        }

        virtual std::string get_statements_prefix(FunctionAst const& ast) const& final override
        {
            return "uint64_t " + _identifier_mangler.mangle_variable(ast.parameter_name) + "{std::stoull(argv[1])};";
        }

        virtual std::string get_return_statement(std::string_view const& expression) const& final override
        {
            return "std::cout<<" + std::string { expression } + "<<std::endl;return 0;";
        }

    private:
        IdentifierMangler const& _identifier_mangler;
    };
}

#endif
