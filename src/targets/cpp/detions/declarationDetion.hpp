#ifndef TARGET_CPP_DECLARATION_DETION_HPP
#define TARGET_CPP_DECLARATION_DETION_HPP

#include <targets/cpp/detion.hpp>

namespace imp
{
    struct DeclarationDetion : public Detion
    {
        using PartialOutput = VisitorStaticMembers<std::string>::PartialOutput;

        virtual PartialOutput get_function_target(
            FunctionAst const& ast,
            ControlflowStrategy const& controlflow_strategy) const& final override
        {
            return PartialOutput::rightOf(controlflow_strategy.get_signature(ast) + ";");
        }
    };
}

#endif
