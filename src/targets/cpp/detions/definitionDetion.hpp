#ifndef TARGET_CPP_DEFINITION_DETION_HPP
#define TARGET_CPP_DEFINITION_DETION_HPP

#include <targets/cpp/visitors/variableDefinitionVisitor.hpp>
#include <targets/cpp/visitors/statementVisitor.hpp>
#include <targets/cpp/detion.hpp>

namespace imp
{
    struct DefinitionDetion : public Detion
    {
        using PartialOutput = VisitorStaticMembers<std::string>::PartialOutput;

        DefinitionDetion(IdentifierMangler const& identifier_mangler):
            _identifier_mangler { identifier_mangler }
        {}

        virtual PartialOutput get_function_target(
            FunctionAst const& ast,
            ControlflowStrategy const& controlflow_strategy) const& final override
        {
            auto const&& body_output { ast.body->accept<StatementVisitor>(_identifier_mangler, controlflow_strategy) };
            if (body_output.isLeft) return PartialOutput::leftOf(body_output.leftValue);

            std::string variable_definitions;
            auto&& variable_definitions_output { ast.body->accept<VariableDefinitionVisitor>(_identifier_mangler) };
            if (!variable_definitions_output.isLeft) variable_definitions = std::move(variable_definitions_output.rightValue);

            std::string const&& body { controlflow_strategy.get_statements_prefix(ast) + variable_definitions + body_output.rightValue };
            return PartialOutput::rightOf(controlflow_strategy.get_signature(ast) + "{" + body + "}");
        }

    private:
        IdentifierMangler const& _identifier_mangler;
    };
}

#endif
