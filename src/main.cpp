#include <targets/common/identifiers/underscoreIdentifierMangler.hpp>
#include <targets/cpp/controlflowStrategies/mainControlflowStrategy.hpp>
#include <targets/cpp/controlflowStrategies/defaultControlflowStrategy.hpp>
#include <targets/cpp/detions/declarationDetion.hpp>
#include <targets/cpp/detions/definitionDetion.hpp>
#include <targets/cpp/visitors/rootVisitor.hpp>
#include <parsers/rootParser.hpp>

#include <iostream>

imp::RootParser::Ast parse_or_exit(imp::RootParser::Input const& input)
{
    auto&& parser_output { imp::root_parser.try_parse(input) };

    if (parser_output.isLeft)
    {
        std::cerr << "expected " << parser_output.leftValue << std::endl;
        exit(1);
    }

    return std::move(parser_output.rightValue.ast);
}

imp::RootVisitor::Output generate_target_or_exit(imp::RootParser::Ast const& ast)
{
    imp::IdentifierMangler const& identifier_mangler { imp::underscore_identifier_mangler };

    imp::MainControlflowStrategy const main_controlflow_strategy { identifier_mangler };
    imp::DefaultControlflowStrategy const default_controlflow_strategy { identifier_mangler };
    std::vector<std::reference_wrapper<imp::ControlflowStrategy const>> const controlflow_strategies
    {
        main_controlflow_strategy,
        default_controlflow_strategy,
    };

    imp::DeclarationDetion const declaration_detion;
    imp::DefinitionDetion const definition_detion { identifier_mangler };
    std::vector<std::reference_wrapper<imp::Detion const>> const detions
    {
        declaration_detion,
        definition_detion,
    };

    auto const&& target_output { ast.accept<imp::RootVisitor>(identifier_mangler, controlflow_strategies, detions) };

    if (target_output.isLeft)
    {
        std::cerr << "expected " << target_output.leftValue << std::endl;
        exit(1);
    }

    return target_output.rightValue;
}

void export_target_and_exit(imp::RootVisitor::Output const& target)
{
    std::cout << target;
    exit(0);
}

int main(int argc, char** argv)
{
    imp::Source const source { argv[1] };
    imp::RootParser::Input const input { source };
    auto const&& ast { parse_or_exit(input) };
    auto const&& target { generate_target_or_exit(ast) };
    export_target_and_exit(target);
}
