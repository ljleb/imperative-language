#ifndef ASSIGNMENT_PARSER_HPP
#define ASSIGNMENT_PARSER_HPP

#include <parsers/expressionParser.hpp>
#include <parsers/tokens/skipParser.hpp>
#include <parsers/combinators/thenParser.hpp>
#include <ast/statementAst.hpp>
#include <ast/statements/assignmentAst.hpp>

#include <parser.hpp>

#include <neither/either.hpp>
#include <memory>

namespace imp
{
    struct AssignmentParser final : public Parser<std::unique_ptr<StatementAst>>
    {
        inline static SkipParser const assignment_token_parser { "=" };

        virtual PartialOutput try_parse(Input const& input) const& final override
        {
            auto&& name_output { name_parser.try_parse(input) };
            if (name_output.isLeft) return PartialOutput::leftOf(name_output.leftValue);

            auto&& token_output { assignment_token_parser.try_parse({ name_output.rightValue.remainder }) };
            if (token_output.isLeft) return PartialOutput::leftOf(token_output.leftValue);

            auto&& expression_output { expression_parser.try_parse({ token_output.rightValue.remainder }) };
            if (expression_output.isLeft) return PartialOutput::leftOf(expression_output.leftValue);

            return PartialOutput::rightOf(Output
            {
                remainder: expression_output.rightValue.remainder,
                ast: std::make_unique<AssignmentAst>(
                    std::move(name_output.rightValue.ast),
                    std::move(expression_output.rightValue.ast)),
            });
        }
    } const assignment_parser;
}

#endif
