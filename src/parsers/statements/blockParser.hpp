#ifndef BLOCK_PARSER_HPP
#define BLOCK_PARSER_HPP

#include <parsers/expressionParser.hpp>
#include <parsers/tokens/skipParser.hpp>
#include <parsers/combinators/thenParser.hpp>
#include <ast/statementAst.hpp>
#include <ast/statements/blockAst.hpp>

#include <parser.hpp>

#include <memory>
#include <vector>

namespace imp
{
    extern Parser<std::unique_ptr<StatementAst>> const& statement_parser;

    struct BlockParser final : public Parser<std::unique_ptr<StatementAst>>
    {
        inline static SkipParser const block_begin_parser { "{" };
        inline static SkipParser const block_end_parser { "}" };

        virtual PartialOutput try_parse(Input const& input) const& final override
        {
            auto&& block_begin_output { block_begin_parser.try_parse(input) };
            if (block_begin_output.isLeft) return PartialOutput::leftOf(block_begin_output.leftValue);

            std::vector<std::unique_ptr<StatementAst const>> statements;
            Source remainder { block_begin_output.rightValue.remainder };

            while (!remainder.starts_with("}"))
            {
                auto&& statement_output { statement_parser.try_parse({ remainder }) };
                if (statement_output.isLeft) return PartialOutput::leftOf(statement_output.leftValue);

                statements.emplace_back(std::move(statement_output.rightValue.ast));
                remainder = statement_output.rightValue.remainder;
            }

            auto&& block_end_output { block_end_parser.try_parse({ remainder }) };
            if (block_end_output.isLeft) return PartialOutput::leftOf(block_end_output.leftValue);

            return PartialOutput::rightOf(Output
            {
                remainder: std::move(block_end_output.rightValue.remainder),
                ast: std::make_unique<BlockAst>(std::move(statements)),
            });
        }
    } const block_parser;
}

#endif
