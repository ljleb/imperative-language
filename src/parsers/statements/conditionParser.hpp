#ifndef CONDITION_PARSER_HPP
#define CONDITION_PARSER_HPP

#include <parsers/expressionParser.hpp>
#include <parsers/tokens/skipParser.hpp>
#include <parsers/combinators/thenParser.hpp>
#include <ast/statementAst.hpp>
#include <ast/statements/conditionAst.hpp>

#include <parser.hpp>

#include <neither/either.hpp>
#include <memory>

namespace imp
{
    extern Parser<std::unique_ptr<StatementAst>> const& statement_parser;

    struct ConditionParser final : public Parser<std::unique_ptr<StatementAst>>
    {
        inline static SkipParser const if_token { "if" };
        inline static SkipParser const else_token { "else" };

        virtual PartialOutput try_parse(Input const& input) const& final override
        {
            auto&& if_token_output { if_token.try_parse(input) };
            if (if_token_output.isLeft) return PartialOutput::leftOf(if_token_output.leftValue);

            auto&& condition_output { expression_parser.try_parse({ if_token_output.rightValue.remainder }) };
            if (condition_output.isLeft) return PartialOutput::leftOf(condition_output.leftValue);

            auto&& true_statement_output { statement_parser.try_parse({ condition_output.rightValue.remainder }) };
            if (true_statement_output.isLeft) return PartialOutput::leftOf(true_statement_output.leftValue);

            auto&& else_token_output { else_token.try_parse({ true_statement_output.rightValue.remainder }) };
            if (else_token_output.isLeft) return PartialOutput::rightOf(Output
            {
                remainder: true_statement_output.rightValue.remainder,
                ast: std::make_unique<ConditionAst>(
                    std::move(condition_output.rightValue.ast),
                    std::move(true_statement_output.rightValue.ast)),
            });

            auto&& false_statement_output { statement_parser.try_parse({ else_token_output.rightValue.remainder }) };
            if (false_statement_output.isLeft) return PartialOutput::leftOf(false_statement_output.leftValue);

            return PartialOutput::rightOf(Output
            {
                remainder: false_statement_output.rightValue.remainder,
                ast: std::make_unique<ConditionAst>(
                    std::move(condition_output.rightValue.ast),
                    std::move(true_statement_output.rightValue.ast),
                    std::move(false_statement_output.rightValue.ast)),
            });
        }
    } const condition_parser;
}

#endif
