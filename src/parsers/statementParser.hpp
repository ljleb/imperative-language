#ifndef STATEMENT_PARSER_HPP
#define STATEMENT_PARSER_HPP

#include <parsers/statements/returnParser.hpp>
#include <parsers/statements/assignmentParser.hpp>
#include <parsers/statements/blockParser.hpp>
#include <parsers/statements/whileParser.hpp>
#include <parsers/statements/conditionParser.hpp>
#include <parsers/combinators/orParser.hpp>
#include <parsers/combinators/thenParser.hpp>

#include <parser.hpp>

#include <memory>

namespace imp
{
    namespace StatementParser
    {
        SkipParser const semicolon_parser { ";" };
        OrParser const semicolon_statement_or_parser { return_parser, assignment_parser };
        ThenParser const semicolon_statement_parser { semicolon_statement_or_parser, semicolon_parser };

        OrParser const impl { block_parser, semicolon_statement_parser, condition_parser, while_parser };
    }

    Parser<std::unique_ptr<StatementAst>> const& statement_parser { StatementParser::impl };
}

#endif
