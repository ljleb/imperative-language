#ifndef COMBINATOR_SIZE_WHILE_PARSER_HPP
#define COMBINATOR_SIZE_WHILE_PARSER_HPP

#include <parser.hpp>

#include <string_view>
#include <string>

namespace imp
{
    template <typename Condition>
    struct SizeWhileParser final : public Parser<std::string_view::size_type>
    {
        SizeWhileParser(Condition const& condition, std::string_view::size_type const& min_size = 0):
            _condition { condition },
            _min_size { min_size }
        {}

        virtual PartialOutput try_parse(Input const& input) const& final override
        {
            std::string_view::size_type const& segment_size
            {
                input.source.find_last_index_where(_condition)
            };

            if (segment_size < _min_size)
            {
                return PartialOutput::leftOf("to match at least " + std::to_string(_min_size) + " characters");
            }

            return PartialOutput::rightOf(Output
            {
                remainder: input.source,
                ast: segment_size,
            });
        }

    private:
        Condition const& _condition;
        std::string_view::size_type _min_size;
    };
}

#endif
