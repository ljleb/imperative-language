#ifndef COMBINATOR_TAKE_WHILE_PARSER_HPP
#define COMBINATOR_TAKE_WHILE_PARSER_HPP

#include "sizeWhileParser.hpp"

#include <parser.hpp>

#include <string_view>

namespace imp
{
    template <typename Condition>
    struct TakeWhileParser final : public Parser<std::string_view>
    {
        TakeWhileParser(Condition const& condition, std::string_view::size_type const& min_size = 0):
            _size_while_parser { condition, min_size }
        {}

        virtual PartialOutput try_parse(Input const& input) const& final override
        {
            auto const&& size_output { _size_while_parser.try_parse(input) };
            if (size_output.isLeft) return PartialOutput::leftOf(size_output.leftValue);

            return PartialOutput::rightOf(Output
            {
                remainder: input.source.advance(size_output.rightValue.ast),
                ast: input.source.segment(size_output.rightValue.ast),
            });
        }

    private:
        SizeWhileParser<Condition> _size_while_parser;
    };
}

#endif
