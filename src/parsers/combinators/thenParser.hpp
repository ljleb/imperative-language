#ifndef COMBINATOR_THEN_PARSER_HPP
#define COMBINATOR_THEN_PARSER_HPP

#include <parser.hpp>

#include <type_traits>
#include <cstdint>

namespace imp
{
    namespace
    {
        template <typename CurrentAst, typename... Asts>
        struct UniqueNonVoid;

        template <typename... Asts>
        struct UniqueNonVoid<void, void, Asts...>
        {
            using type = typename UniqueNonVoid<void, Asts...>::type;
        };

        template <typename FoundAst, typename... Asts>
        struct UniqueNonVoid<void, FoundAst, Asts...>
        {
            using type = typename UniqueNonVoid<FoundAst, Asts...>::type;
        };

        template <typename FoundAst, typename... Asts>
        struct UniqueNonVoid<FoundAst, void, Asts...>
        {
            using type = typename UniqueNonVoid<FoundAst, Asts...>::type;
        };

        template <>
        struct UniqueNonVoid<void>
        {};

        template <typename FoundAst>
        struct UniqueNonVoid<FoundAst>
        {
            using type = FoundAst;
        };

        template <typename... Asts>
        using UniqueNonVoidIn = typename UniqueNonVoid<void, Asts...>::type;
    }

    template <typename... Parsers>
    struct ThenParser final : public Parser<UniqueNonVoidIn<typename Parsers::Ast...>>
    {
        ThenParser(Parsers const&... parsers):
            _parsers { parsers... }
        {}

        virtual typename ThenParser::PartialOutput try_parse(typename ThenParser::Input const& input) const& final override
        {
            return try_parse_all<0>(input);
        }

    private:
        std::tuple<Parsers const&...> _parsers;

        template <uint64_t index>
        using ParserAt = std::tuple_element_t<index, std::tuple<Parsers...>>;

        template <int64_t index>
        typename ThenParser::PartialOutput try_parse_all(typename ThenParser::Input const& input) const&
        {
            auto&& partial_output { std::get<index>(_parsers).try_parse(input) };
            if (partial_output.isLeft) return ThenParser::PartialOutput::leftOf(partial_output.leftValue);

            if constexpr (index + 1 >= sizeof...(Parsers))
            {
                return std::move(partial_output);
            }
            else if constexpr (std::is_same_v<typename ParserAt<index>::Ast, typename ThenParser::Ast>)
            {
                return try_parse_after_ast<index + 1>(std::move(partial_output.rightValue));
            }
            else
            {
                return try_parse_all<index + 1>({ partial_output.rightValue.remainder });
            }
        }

        template <uint64_t index>
        typename ThenParser::PartialOutput try_parse_after_ast(typename ThenParser::Output&& previous_output) const&
        {
            auto&& partial_output { std::get<index>(_parsers).try_parse({ previous_output.remainder }) };
            if (partial_output.isLeft) return ThenParser::PartialOutput::leftOf(partial_output.leftValue);

            typename ThenParser::Output next_output
            {
                remainder: partial_output.rightValue.remainder,
                ast: std::move(previous_output.ast),
            };

            if constexpr (index + 1 < sizeof...(Parsers))
            {
                return try_parse_after_ast<index + 1>(std::move(next_output));
            }
            else
            {
                return ThenParser::PartialOutput::rightOf(std::move(next_output));
            }
        }
    };
}

#endif
