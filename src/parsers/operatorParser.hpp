#ifndef OPERATOR_PARSER_HPP
#define OPERATOR_PARSER_HPP

#include <parsers/operators/plusParser.hpp>
#include <parsers/operators/minusParser.hpp>
#include <parsers/operators/timesParser.hpp>
#include <parsers/operators/divideParser.hpp>
#include <parsers/combinators/orParser.hpp>
#include <ast/operatorAst.hpp>

#include <parser.hpp>

#include <memory>

namespace imp
{
    namespace OperatorParser
    {
        OrParser const impl { plus_parser, minus_parser, times_parser, divide_parser };
    }
    Parser<std::unique_ptr<OperatorAst>> const& operator_parser { OperatorParser::impl };
}

#endif
