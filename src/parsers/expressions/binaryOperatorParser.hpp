#ifndef BINARY_OPERATOR_PARSER_HPP
#define BINARY_OPERATOR_PARSER_HPP

#include <parsers/operatorParser.hpp>
#include <parsers/combinators/thenParser.hpp>
#include <ast/expressions/binaryOperatorAst.hpp>

#include <parser.hpp>

namespace imp
{
    extern Parser<std::unique_ptr<ExpressionAst>> const& expression_parser;
    extern Parser<std::unique_ptr<ExpressionAst>> const& atom_expression_parser;

    struct BinaryOperatorParser final : public Parser<std::unique_ptr<ExpressionAst>>
    {
        virtual PartialOutput try_parse(Input const& input) const& final override
        {
            auto&& left_output { atom_expression_parser.try_parse(input) };
            if (left_output.isLeft) return PartialOutput::leftOf(left_output.leftValue);

            auto&& operator_output { operator_parser.try_parse({ left_output.rightValue.remainder }) };
            if (operator_output.isLeft) return PartialOutput::leftOf(operator_output.leftValue);

            auto&& right_output { expression_parser.try_parse({ operator_output.rightValue.remainder }) };
            if (right_output.isLeft) return PartialOutput::leftOf(right_output.leftValue);

            return PartialOutput::rightOf(Output
            {
                remainder: right_output.rightValue.remainder,
                ast: std::make_unique<BinaryOperatorAst>(
                    std::move(operator_output.rightValue.ast),
                    std::move(left_output.rightValue.ast),
                    std::move(right_output.rightValue.ast)),
            });
        }
    } const binary_operator_parser;
}

#endif
