#ifndef REFERENCE_PARSER_HPP
#define REFERENCE_PARSER_HPP

#include <parsers/tokens/nameParser.hpp>
#include <ast/expressions/referenceAst.hpp>
#include <ast/expressionAst.hpp>

#include <parser.hpp>

namespace imp
{
    struct ReferenceParser final : public Parser<std::unique_ptr<ExpressionAst>>
    {
        virtual PartialOutput try_parse(Input const& input) const& final override
        {
            auto&& name_output { name_parser.try_parse(input) };
            if (name_output.isLeft) return PartialOutput::leftOf(name_output.leftValue);

            return PartialOutput::rightOf(Output
            {
                remainder: name_output.rightValue.remainder,
                ast: std::make_unique<ReferenceAst>(std::move(name_output.rightValue.ast)),
            });
        }
    } const reference_parser;
}

#endif
