#ifndef CALL_PARSER_HPP
#define CALL_PARSER_HPP

#include <parsers/tokens/skipParser.hpp>
#include <parsers/tokens/nameParser.hpp>
#include <ast/expressions/callAst.hpp>
#include <ast/expressionAst.hpp>

#include <parser.hpp>

#include <memory>

namespace imp
{
    extern Parser<std::unique_ptr<ExpressionAst>> const& expression_parser;

    struct CallParser final : public Parser<std::unique_ptr<ExpressionAst>>
    {
        inline static SkipParser const open_parenthesis_parser { "(" };
        inline static SkipParser const closed_parenthesis_parser { ")" };

        virtual PartialOutput try_parse(Input const& input) const& final override
        {
            auto&& name_output { name_parser.try_parse(input) };
            if (name_output.isLeft) return PartialOutput::leftOf(name_output.leftValue);

            auto const&& open_parenthesis_ouput { open_parenthesis_parser.try_parse({ name_output.rightValue.remainder }) };
            if (open_parenthesis_ouput.isLeft) return PartialOutput::leftOf(open_parenthesis_ouput.leftValue);

            auto&& argument_output { expression_parser.try_parse({ open_parenthesis_ouput.rightValue.remainder }) };
            if (argument_output.isLeft) return PartialOutput::leftOf(argument_output.leftValue);

            auto const&& closed_parenthesis_ouput { closed_parenthesis_parser.try_parse({ argument_output.rightValue.remainder }) };
            if (closed_parenthesis_ouput.isLeft) return PartialOutput::leftOf(closed_parenthesis_ouput.leftValue);

            return PartialOutput::rightOf(Output
            {
                remainder: closed_parenthesis_ouput.rightValue.remainder,
                ast: std::make_unique<CallAst>(
                    std::move(name_output.rightValue.ast),
                    std::move(argument_output.rightValue.ast)),
            });
        }
    } const call_parser;
}

#endif
