#ifndef NUMBER_PARSER_HPP
#define NUMBER_PARSER_HPP

#include <parsers/tokens/spaceParser.hpp>
#include <parsers/combinators/takeWhileParser.hpp>
#include <parsers/combinators/thenParser.hpp>
#include <ast/expressions/numberAst.hpp>
#include <ast/expressionAst.hpp>

#include <parser.hpp>

#include <cctype>

namespace imp
{
    struct NumberParser final : public Parser<std::unique_ptr<ExpressionAst>>
    {
        inline static TakeWhileParser const digit_parser
        {
            [](unsigned char character) { return std::isdigit(character); },
            1
        };
        inline static ThenParser const digit_space_parser { digit_parser, space_parser };
 
        virtual PartialOutput try_parse(Input const& input) const& final override
        {
            auto const& digits_output { digit_space_parser.try_parse(input) };
            if (digits_output.isLeft) return PartialOutput::leftOf(digits_output.leftValue);

            std::string const dumb_digits_copy { digits_output.rightValue.ast };
            uint64_t&& number { std::stoull(dumb_digits_copy) };

            return PartialOutput::rightOf(Output
            {
                remainder: digits_output.rightValue.remainder,
                ast: std::make_unique<NumberAst>(std::move(number)),
            });
        }
    } const number_parser;
}

#endif
