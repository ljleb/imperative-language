#ifndef TIMES_PARSER
#define TIMES_PARSER

#include <ast/operators/timesOperatorAst.hpp>
#include <parsers/operators/operatorImplParser.hpp>
#include <ast/operatorAst.hpp>

namespace imp
{
    struct TimesParser final : public OperatorParserImpl<TimesParser, TimesOperatorAst>
    {
        static char const* get_token()
        {
            return "*";
        }
    } const times_parser;
}

#endif
