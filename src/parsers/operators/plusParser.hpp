#ifndef PLUS_PARSER
#define PLUS_PARSER

#include <ast/operators/plusOperatorAst.hpp>
#include <ast/operatorAst.hpp>
#include <parsers/operators/operatorImplParser.hpp>

namespace imp
{
    struct PlusParser final : public OperatorParserImpl<PlusParser, PlusOperatorAst>
    {
        static char const* get_token()
        {
            return "+";
        }
    } const plus_parser;
}

#endif
