#ifndef DIVIDE_PARSER
#define DIVIDE_PARSER

#include <ast/operators/divideOperatorAst.hpp>
#include <parsers/operators/operatorImplParser.hpp>
#include <ast/operatorAst.hpp>

namespace imp
{
    struct DivideParser final : public OperatorParserImpl<DivideParser, DivideOperatorAst>
    {
        static char const* get_token()
        {
            return "/";
        }
    } const divide_parser;
}

#endif
