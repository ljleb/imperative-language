#ifndef MINUS_PARSER
#define MINUS_PARSER

#include <ast/operators/minusOperatorAst.hpp>
#include <parsers/operators/operatorImplParser.hpp>
#include <ast/operatorAst.hpp>

namespace imp
{
    struct MinusParser final : public OperatorParserImpl<MinusParser, MinusOperatorAst>
    {
        static char const* get_token()
        {
            return "-";
        }
    } const minus_parser;
}

#endif
