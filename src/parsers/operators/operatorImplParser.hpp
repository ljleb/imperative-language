#ifndef OPERATOR_IMPL_PARSER_HPP
#define OPERATOR_IMPL_PARSER_HPP

#include <parsers/tokens/skipParser.hpp>
#include <parser.hpp>

#include <memory>

namespace imp
{
    template <typename _This, typename _Ast>
    struct OperatorParserImpl : public Parser<std::unique_ptr<OperatorAst>>
    {
        inline static SkipParser token_parser { _This::get_token() };

        virtual PartialOutput try_parse(Input const& input) const& final override
        {
            auto&& token_output { token_parser.try_parse(input) };
            if (token_output.isLeft) return PartialOutput::leftOf(token_output.leftValue);

            return PartialOutput::rightOf(Output
            {
                remainder: token_output.rightValue.remainder,
                ast: std::make_unique<_Ast>(),
            });
        }
    };
}

#endif
