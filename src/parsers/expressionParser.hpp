#ifndef EXPRESSION_PARSER_HPP
#define EXPRESSION_PARSER_HPP

#include <parsers/expressions/numberParser.hpp>
#include <parsers/expressions/referenceParser.hpp>
#include <parsers/expressions/binaryOperatorParser.hpp>
#include <parsers/expressions/callParser.hpp>
#include <parsers/combinators/orParser.hpp>
#include <parsers/combinators/thenParser.hpp>
#include <ast/expressionAst.hpp>

#include <parser.hpp>

#include <memory>

namespace imp
{
    namespace ExpressionParser
    {
        OrParser const atom_impl { number_parser, call_parser, reference_parser };
        OrParser const impl { binary_operator_parser, atom_impl };
    }
    Parser<std::unique_ptr<ExpressionAst>> const& atom_expression_parser { ExpressionParser::atom_impl };
    Parser<std::unique_ptr<ExpressionAst>> const& expression_parser { ExpressionParser::impl };
}

#endif
