#ifndef ROOT_PARSER_HPP
#define ROOT_PARSER_HPP

#include <parsers/functionParser.hpp>
#include <ast/rootAst.hpp>

#include <parsers/tokens/spaceParser.hpp>

#include <parser.hpp>

#include <neither/either.hpp>
#include <memory>

namespace imp
{
    struct RootParser final : public Parser<RootAst>
    {
        virtual PartialOutput try_parse(Input const& input) const& final override
        {
            auto const&& space_output { space_parser.try_parse(input) };
            std::vector<std::unique_ptr<FunctionAst const>> functions;
            Source remainder { space_output.rightValue.remainder };

            while (remainder.not_empty())
            {
                FunctionParser::PartialOutput&& function_output { function_parser.try_parse({ remainder }) };
                if (function_output.isLeft) return PartialOutput::leftOf(function_output.leftValue);

                functions.emplace_back(std::move(function_output.rightValue.ast));
                remainder = function_output.rightValue.remainder;
            }

            return PartialOutput::rightOf(Output
            {
                remainder: remainder,
                ast: { std::move(functions) },
            });
        }
    } const root_parser;
}

#endif
