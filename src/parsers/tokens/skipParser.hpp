#ifndef SKIP_PARSER_HPP
#define SKIP_PARSER_HPP

#include "spaceParser.hpp"

#include <parser.hpp>

#include <neither/either.hpp>
#include <string_view>
#include <string>

namespace imp
{
    struct SkipParser final : public Parser<void>
    {
        SkipParser(std::string_view const& to_skip):
            _to_skip { to_skip }
        {}

        virtual PartialOutput try_parse(Input const& input) const& final override
        {
            if (!input.source.starts_with(_to_skip))
            {
                return PartialOutput::leftOf("'" + std::string { _to_skip } + "'");
            }

            Input const& remainder { input.source.advance(_to_skip.size()) };

            auto const& space_output { space_parser.try_parse(remainder) };
            return space_output;
        }

    private:
        std::string_view _to_skip;
    };
}

#endif
