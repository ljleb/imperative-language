include make/tree.mk

CXX := g++
CXX_STD := --std=c++2a
CXX_INCLUDES := -I./$(SRC_DIRECTORY) -I./$(LIB_DIRECTORY)
CXX_OPTIONS := -Wall

# release variables
CXX_release_OPTIONS := $(CXX_STD) $(CXX_OPTIONS) -O3 $(CXX_INCLUDES)

# debug variables
CXX_debug_OPTIONS := -g $(CXX_STD) $(CXX_OPTIONS) -O0  $(CXX_INCLUDES)
