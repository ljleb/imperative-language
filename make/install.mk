.PHONY: install uninstall

include make/tree.mk

PREFIX := /usr/local

install: $(PREFIX)/bin/imp $(PREFIX)/bin/impcli

uninstall:
	rm -f $(PREFIX)/bin/imp $(PREFIX)/bin/impcli

$(PREFIX)/bin/imp: $(OUT_DIRECTORY)/release/main
	cp -f $^ $@

$(PREFIX)/bin/impcli: scripts/cli
	cp -f $^ $@ && \
	chmod +x $@
