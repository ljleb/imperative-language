.PHONY: release debug

include make/options.mk
include make/tree.mk

release: $(OUT_DIRECTORY)/release/main
debug: out/debug/main

$(OUT_DIRECTORY)/%/main: $(OUT_DIRECTORY)/%/main.o
	$(CXX) $(CXX_$*_OPTIONS) -o $@ $^

$(OUT_DIRECTORY)/%/main.o: $(SRC_DIRECTORY)/main.cpp $(HEADERS) | out/%/
	$(CXX) -c $(CXX_$*_OPTIONS) -o $@ $<

$(OUT_DIRECTORY)/%/:
	mkdir -p $@
